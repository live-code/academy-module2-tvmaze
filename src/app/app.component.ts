import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Series, Show} from "./model/series";

@Component({
  selector: 'ac-root',
  template: `

    <form #f="ngForm" (submit)="submit(f.value)">
      <input type="text" ngModel name="text" placeholder="Search TV Series">
    </form>

    <div class="grid">
      <div class="grid-item" *ngFor="let series of result">
        <div class="movie" (click)="itemClickHandler(series)">
          <img *ngIf="series.show.image" [src]="series.show.image.medium" alt="">
          <div *ngIf="!series.show.image" class="noImage"></div>
          <div class="movieText">{{series.show.name}}</div>
        </div>
      </div>
    </div>

    <div class="wrapper" *ngIf="selectedSeries">
      <div class="content">
          <span
            class="closeButton"
            aria-label="Close"
            (click)="selectedSeries = null"
          >×</span>

        <img *ngIf="selectedSeries.image" class="image" [src]="selectedSeries.image.original" alt="">

        <div class="metadata">
          <h1>{{selectedSeries.name}}</h1>
          <span
            class="tag"
            *ngFor="let genres of selectedSeries.genres"
          >{{genres}}</span>

          <div [innerHTML]="selectedSeries.summary">xxx</div>

          <a [href]="selectedSeries.url" target="_blank" class="button">Go to webpage</a>
        </div>
      </div>
    </div>
  `,
  styleUrls: [`./app.component.css`]
})
export class AppComponent implements OnInit {
  result: Series[] | null = null;
  selectedSeries: Show | null = null;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.search('soprano')
  }

  submit(formData: any) {
    this.search(formData.text)
  }

  search(text: string) {
    this.http.get<Series[]>(`https://api.tvmaze.com/search/shows?q=${text}` )
      .subscribe(res => {
        this.result = res;
      })
  }

  itemClickHandler(series: Series) {
    this.selectedSeries = series.show;
  }
}
